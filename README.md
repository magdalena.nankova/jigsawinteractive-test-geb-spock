# Geb + Spock Selenium UI Test Project for https://app.jigsawinteractive.com

## Description

This project provides a solution to interview process QA assignment | Prime Holding. 

Please follow the steps:
```
1. Setup a new Spock with Geb project
2. Create a test for "First-time user" scenario
2.1 Go to https://app.dev.jigsawinteractive.com/
2.2 Click the "First time user" button
2.3 Verify that Login page is open
2.4 Enter a valid email address
2.5 Click the "Continue" button
2.6 Verify that My Profile page is open
- Assert that your email address is pre-populated in the "Email address" field
2.7 Fill the required fields
2.8 Click the "Save" button
2.9 Assert that the Home page (My Sessions) is open and the session list is empty
```

## Usage

The following commands will launch the tests with the individual browsers:

    ./gradlew chromeTest
    ./gradlew firefoxTest

To run with all, you can run:

    ./gradlew test

Replace `./gradlew` with `gradlew.bat` in the above examples if you're on Windows.
