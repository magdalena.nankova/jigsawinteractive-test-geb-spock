import geb.spock.GebSpec

class FirstTimeUserSpec extends GebSpec {

    def "register First Time User test scenario"() {
        when:
        def homePage = to HomePage

        and:
        at HomePage

        and:
        homePage.btnFirstTimeUser.click()

        then:
        def loginPage = at LoginPage

        when:
        def randomValue = UUID.randomUUID().toString()
        def emailValue = "magdalena.nankova+" + randomValue + "@gmail.com"
        loginPage.txtEmail = emailValue

        and:
        loginPage.btnContinue.click()

        then:
        sleep(5000)
        def myProfilePage = at MyProfilePage

        and:
        myProfilePage.EmailAddress == emailValue

        when:
        def passwordValue = "abcABC123"
        myProfilePage.FirstName = "Magdalena"
        myProfilePage.LastName = "Nankova"
        myProfilePage.Password = passwordValue
        myProfilePage.ConfirmPassword = passwordValue
        sleep(5000)
        myProfilePage.CountryId = "Bulgaria"
        sleep(5000)
        myProfilePage.RegionId = "Sofiya"
        sleep(5000)
        myProfilePage.CityId = "Sofiya"

        and:
        sleep(5000)
        myProfilePage.SaveButton.click()

        then:
        sleep(5000)
        def mySessionsPage = at MySessionsPage

        and:
        $("#listSessionsUpcoming > *").size() == 0
    }
}