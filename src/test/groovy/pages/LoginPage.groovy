import geb.Page
import org.openqa.selenium.By

class LoginPage extends Page {

    static at = { title == "Virtual Learning - Login" && welcomeElement.isDisplayed() }

    static content = {
    	welcomeElement { $(By.xpath("//*[@id='signinForm']//strong[contains(@class, 'first-time-user-visible') and text()='Welcome! Please enter your email address to continue.']")) }
    	txtEmail { $(By.id("txtEmail")) }
    	btnContinue(to: MyProfilePage) { $(By.id("btnContinue")) }
    }

}
