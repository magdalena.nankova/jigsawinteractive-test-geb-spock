import geb.Page
import org.openqa.selenium.By

class MyProfilePage extends Page {

    static at = { title == "Virtual Learning - Register" }

    static content = {
    	EmailAddress { $(By.id("EmailAddress")) }
    	FirstName {$(By.id("FirstName")) }
    	LastName {$(By.id("LastName")) }
    	Password {$(By.id("Password")) }
    	ConfirmPassword {$(By.id("ConfirmPassword")) }
    	SaveButton() {$(By.id("saveButton")) }
    	CountryId() {$(By.xpath("//input[@type='text' and @name='CountryId_input']")) }
    	RegionId() {$(By.xpath("//input[@type='text' and @name='RegionId_input']")) }
    	CityId() {$(By.xpath("//input[@type='text' and @name='CityId_input']")) }
    }

}
