import geb.Page
import org.openqa.selenium.By

class MySessionsPage extends Page {

    static at = { title == "Virtual Learning - My Sessions" }
}
