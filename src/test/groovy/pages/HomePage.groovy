import geb.Page
import org.openqa.selenium.By

class HomePage extends Page {

    static at = { title == "Virtual Learning - Login" }

    static content = {
    	btnFirstTimeUser(to: LoginPage) { $(By.id("btnFirstTimeUser")) }
    }
}
